import React, { useState, useEffect } from "react";
import "./Contacts.css";
import ListUsers from "./ListUsers";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSearch } from "@fortawesome/free-solid-svg-icons";

const Contacts = () => {
  const [usersList, setUsersList] = useState([]);
  const [userSearch, setUserSearch] = useState("");
  const [filteredUsers, setFilteredUsers] = useState([]);
  const [selectedUsers, setSelectedUsers] = useState([]);

  useEffect(() => {
    const fetchUsers = async () => {
      try {
        const response = await fetch(
          "http://teacode-recruitment-challenge.s3.eu-central-1.amazonaws.com/users.json"
        );
        const data = await response.json();
        const sortedUsers = data.sort((a, b) =>
          a.last_name.localeCompare(b.last_name)
        );
        setUsersList(sortedUsers);
        setFilteredUsers(sortedUsers);
      } catch (error) {}
    };
    fetchUsers();
  }, []);

  const selectUser = (id) => {
    const userIndex = usersList.findIndex((user) => user.id === id);
    if (userIndex > -1) {
      const list = usersList;
      const filteredList = [...filteredUsers];
      const selectedList = selectedUsers;

      if (!list[userIndex].checked) {
        list[userIndex] = { ...list[userIndex], checked: true };
        const filtredIndex = filteredList.findIndex((user) => user.id === id);
        filteredList[filtredIndex] = {
          ...filteredList[filtredIndex],
          checked: true,
        };
        console.log(filteredList[filtredIndex]);
        selectedList.push(list[userIndex].id);
      } else {
        list[userIndex] = { ...list[userIndex], checked: false };
        const filtredIndex = filteredList.findIndex((user) => user.id === id);
        filteredList[filtredIndex] = {
          ...filteredList[filtredIndex],
          checked: false,
        };
        const selectedUser = selectedList.findIndex((user) => user.id === id);
        selectedList.splice(selectedUser, 1);
      }
      setUsersList(list);
      setFilteredUsers(filteredList);
      setSelectedUsers(selectedList);

      console.log(selectedUsers);
    }
  };

  const searchUsers = (event) => {
    const searchValue = event.target.value;
    setUserSearch(searchValue);
    const filteredUser = usersList.filter((user) => {
      const userFullName = user.first_name + " " + user.last_name;
      return userFullName.includes(searchValue);
    });

    setFilteredUsers(filteredUser);
  };

  return (
    <div className="contacts">
      <header className="contacts__header">Contacts</header>
      <div className="contacts__search">
        <FontAwesomeIcon icon={faSearch} color="#999" />
        <input
          className="contacts__search__field"
          placeholder="Search user"
          onChange={searchUsers}
          value={userSearch}
        />
      </div>
      <ListUsers usersList={filteredUsers} selectUser={selectUser} />
    </div>
  );
};

export default Contacts;
