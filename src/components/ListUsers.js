import React from "react";
import User from "./User";

const ListUsers = (props) => {
  console.log(props.usersList);
  const list = props.usersList.map((user) => (
    <User user={user} key={user.id} selectUser={props.selectUser} />
  ));
  return <div className="listuser">{list}</div>;
};

export default ListUsers;
