import React from "react";

const User = (props) => {
  const {
    id,
    first_name,
    last_name,
    email,
    gender,
    avatar,
    checked,
  } = props.user;

  return (
    <div
      className={`${checked ? "selecteduser" : ""} user`}
      onClick={() => props.selectUser(id)}
    >
      <img className="user_avatar" src={avatar} alt="" />
      <div className="user__info">
        {" "}
        <div>
          <span>{first_name} </span>
          <span>{last_name}</span>
        </div>
        <div>
          <span>{email}</span>
        </div>
      </div>
    </div>
  );
};

export default User;
